// clients swiper
var mySwiper = new Swiper('.swiper-container', {
    direction: 'horizontal',
    loop: true,
    spaceBetween: 150,
   
    autoplay: {
        delay: 1000,
        disableOnInteraction: false,
    },
    speed: 800, 
    slidesPerView: 4,
     // Responsive breakpoints
 breakpoints: {
    // when window width is >= 768px
    100: {
        slidesPerView: 1,
    },
    570:{
        slidesPerView: 2,
    },
    850: {
        slidesPerView: 3,
    },
    1200: {
        slidesPerView: 4,
    }
 },
 });
 